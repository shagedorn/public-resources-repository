**INFO**

This project is not maintained and may include outdated information or code that is
not compatible with recent versions of tools, such as, Xcode and CocoaPods. The
repository is online for reference only.

For current projects, please visit my [GitHub](https://github.com/shagedorn) or
[Stackoverflow](https://stackoverflow.com/users/2050985/hagi) profiles.

The repository only contains downloadable resources which are used by [another
project](https://bitbucket.org/shagedorn/modular-erp-app).
